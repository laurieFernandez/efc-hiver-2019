
let isError = false;

const errorMsg = document.querySelector("#js-erreur");
const modal = document.querySelector("#js-modale");
const modalCloseBtn = document.querySelector(".close-btn");

//Valider le nom et prenom
const userName = document.getElementById("nom");
userName.addEventListener("blur", validateName);

function validateName() {

    let regexNom = /^([a-zA-Z]{5,})([0-9]*)$/;

    if (userName.value.match(regexNom)) {
        userName.style.border = "solid 1px #ccc"
        userName.style.color = "black";

    } else {
        userName.style.borderColor = "red";
        userName.style.color = "red";
        isError = true;
    }
}

const prenom = document.getElementById("prenom");
prenom.addEventListener("blur", validatePrenom);

function validatePrenom() {

    let regexNom = /^([a-zA-Z]{5,})([0-9]*)$/;

    if (prenom.value.match(regexNom)) {
        prenom.style.border = "solid 1px #ccc"
        prenom.style.color = "black";

    } else {
        prenom.style.borderColor = "red";
        prenom.style.color = "red";
        isError = true;
    }
}

const numero = document.querySelector("#numero");
numero.addEventListener("blur", validatePhone);
/**
 * Le script match : 
 * XXX-XXX-XXXX
 * XXX.XXX.XXXX
 * XXX XXX XXXX
 * (XXX) XXX XXXX
 */
function validatePhone() {
    let regexNumero = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    if (numero.value.match(regexNumero)) {
        numero.style.border = "solid 1px #ccc"
        numero.style.color = "black";
    } else {
        numero.style.borderColor = "red";
        numero.style.color = "red";
        isError = true;
    }

}

/*Valider mail */
const mail = document.querySelector("#courriel");
mail.addEventListener("blur", validateMail);

function validateMail() {
    let mailRegex = /^([a-zA-Z._-]*)\@([a-z]{3,}).(com|fr|ca|qc.ca)$/;

    let mailRegexInternet = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (mail.value.match(mailRegexInternet)) {
        mail.style.border = "solid 1px #ccc"
        mail.style.color = "black";
    } else {
        mail.style.borderColor = "red";
        mail.style.color = "red";
        isError = true;
    }

}


/* Valider message */
const message = document.querySelector("#message");
function validateMessage() {

    let regexMessage = /[a-zA-Z\&,.?!"$\(\)0-9]/;
    if (message.value.match(regexMessage)) {
        message.style.border = "solid 1px #ccc"
        message.style.color = "black";
    } else {
        message.style.borderColor = "red";
        message.style.color = "red";
        isError = true;
    }
}

//Formulaire
const formulaire = document.querySelector("#js-form");
formulaire.addEventListener("submit", function (e) {
    isError = false;
    e.preventDefault();
    validateName();
    validatePrenom();
    validatePhone();
    validateMail();
    validateMessage();
    if (isError) {
        errorMsg.style.display = "flex";
    } else {
        errorMsg.style.display = "none";
        afficherModale();
    }
});

/* simulation d'envoie */

function afficherModale() {
    modal.style.display = "flex";
    userName.value = "";
    prenom.value = "";
    numero.value = "";
    mail.value = "";
    message.value = "";
}



modalCloseBtn.addEventListener("click", function () {
    modal.style.display = "none";
});

modal.addEventListener("click", function (evt) {
    if (evt.target.id == "js-modale") {
        modal.style.display = "none";
    }
})