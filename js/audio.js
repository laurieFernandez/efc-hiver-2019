let btnMusic = document.querySelectorAll(".btn-musique");
let titre = document.querySelector("#titre-musique");
const modal = document.querySelector("#js-modale");
const modalCloseBtn = document.querySelector(".close-btn");
let audio = document.querySelector("#audioPlayer");
let audios = [{
    nom: "Symphonie de Bethoveen",
    src: "audio/bethoveen.mp3"
}, {
    nom : "Symphonie de Mozart",
    src : "audio/mozart.mp3"
},{
    nom : "Boléro de Ravel",
    src : "audio/bolero.mp3"
},{
    nom : "Spring Waltz de Chopin",
    src : "audio/chopin.mp3"
}];

//------------------------------------------------------------------------//
btnMusic.forEach(function (item) {
    item.addEventListener("click", function () {
        modal.style.display = "flex";
        audio.src = audios[item.dataset.id].src;
        titre.textContent = audios[item.dataset.id].nom;
    })
})
//------------------------------------------------------------------------//


modalCloseBtn.addEventListener("click", function () {
    modal.style.display = "none";
    rejouer(audio);
});

//------------------------------------------------------------------------//

modal.addEventListener("click", function (evt) {
    if (evt.target.id == "js-modale") {
        modal.style.display = "none";
        rejouer(audio);
    }
})

//------------------------------------------------------------------------//

/**
 * Suivi d'un tuto 
 * https://openclassrooms.com/fr/courses/1916641-dynamisez-vos-sites-web-avec-javascript/1921854-laudio-et-la-video
 */

let btnPlay = document.querySelector("#btn-play");
let btnStop = document.querySelector("#btn-stop");

function jouerMusique() {

    if (audio.paused) {
        audio.play();
        btnPlay.textContent = 'Pause';
    } else {
        audio.pause();
        btnPlay.textContent = 'Play';
    }
}

function rejouer() {

    audio.currentTime = 0;
    audio.pause();
    btnPlay.textContent = "Play";
}

function actualiserTemps() {
    let duree = audio.duration;    // Durée totale
    let temps = audio.currentTime; // Temps écoulé
    let fraction = temps / duree;
    let pourcentage = Math.ceil(fraction * 100);

    let progression = document.querySelector('#js-progression-bar');

    progression.style.width = pourcentage + '%';
    progression.textContent = pourcentage + '%';

    if(audio.currentTime == 0){
        progression.textContent = 0 + '%';
    }
    document.querySelector('#js-progression-temps').textContent = formatTime(temps);
}

//-------Fonction helper pour le format du temps de la musique------//
function formatTime(time) {
    let hours = Math.floor(time / 3600);
    let mins = Math.floor((time % 3600) / 60);
    let secs = Math.floor(time % 60);

    if (secs < 10) {
        secs = "0" + secs;
    }

    if (hours) {
        if (mins < 10) {
            mins = "0" + mins;
        }

        return hours + ":" + mins + ":" + secs; // hh:mm:ss
    } else {
        return mins + ":" + secs; // mm:ss
    }
}

