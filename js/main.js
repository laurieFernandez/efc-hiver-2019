
/**
 * Faire une class keyframes et l'ajouter a l'element qui 'saffiche sur le viewport
 */
console.log("script Loaded");

document.addEventListener("DOMContentLoaded", function () {

  const timelineContainer = document.querySelector(".timeline-container");

  // Helper servant à véfifier que l'élément apparait dans le viewport
  function isElementInViewport(el) {
    if (typeof jQuery === "function" && el instanceof jQuery) {
      el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
      rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
  }



  function timeline() {
    let elementsTimeline = document.querySelectorAll(".timeline-item");
    let activeClass = "timeline-item--active"

    if (screen.width <= 850) {
      elementsTimeline.forEach(function (item) {
        console.log(item);
        item.classList.add(activeClass);
      })
    } else {
      window.addEventListener("scroll", function () {
        elementsTimeline.forEach(function (i) {
          //console.log(i.dataset.id);
          if (isElementInViewport(i)) {
            i.classList.add(activeClass);
            console.log(timelineContainer);
          } else {
            i.classList.remove(activeClass);
          }
        });
      });
    }
  };
  timeline();

});


// ----------------------
