// Récupération des éléments du DOM

let virtuoseContainer = document.querySelector(".grandVirtuosesContainer");
let artistesBox = document.querySelector("#js-virtuoses");
let photosArtistes = document.querySelector(".imageArtistes");
let liens = document.querySelector("#js-link");
let artistesArray;
let notesArtistes = [];
let nomItem = "notesArtistes";
let note = 0;
let dataArtiste;

init();

/***** Initialiser le localStorage */
function init() {
    if (localStorage.getItem(nomItem) == null) {
        localStorage.setItem(nomItem, "[]");
    }
    else {
        notesArtistes = JSON.parse(localStorage.getItem(nomItem));
    }
}

/*************** Afficher les artistes ************** */

function loadJSON(url, callback) {

    // Créer un objet XMLHttpRequest
    const xhr = new XMLHttpRequest();

    // Surveiller les changements de status

    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.responseText);
            callback(data);
        }
    }

    xhr.open("GET", url, true);
    xhr.send();
}

// Appeler la fonction callback du JSON

loadJSON("data/virtuoses.json", showArtistes);

// Récupération des données du JSON

function showArtistes(data) {
    dataArtiste = data;
    affichage(data);
}

// Fonction nécessaire pour afficher les éléments sur ma page

function creationDesElements(type, conteneur, style) {
    let b = document.createElement(type);
    conteneur.appendChild(b);
    if (style != "") {
        b.classList.add(style);
    }
    return b;
}

// Création des éléments

// Je crée une variable qui va être = à creationDesElements qui a bsn de 3 paramètre pour fonctionner. 
// Création du type d'élément qu'on veut créer. 
// Rattacher l'élément créé à un conteneur.
// On choisi le style que l'on veut appliquer à notre élément
/* Faire pour chaque éléments avec un forEach */
// Parcourir tous les éléments du array faisant partie du JSON

function affichage(data) {
    artistesArray = data.artistes;
    artistesArray.forEach(function (o) {
        let conteneurArticle = creationDesElements("section", virtuoseContainer, "grandsVirtuoses");
        let conteneurImage = creationDesElements("div", conteneurArticle, "js-virtuoses");
        let image = creationDesElements("img", conteneurImage, "imageArtistes");
        image.src = o.img;
        image.alt= o.alt;
        let conteneurNom = creationDesElements("h2", conteneurImage, "");
        conteneurNom.textContent = o.nom;
        let conteneurDescription = creationDesElements("p", conteneurImage, "");
        conteneurDescription.textContent = o.description;
        let conteneurLien = creationDesElements("p", conteneurImage
            , "link");
        conteneurLien.textContent = "En savoir plus";
        conteneurLien.addEventListener("click", function () {
            window.open(o.lien, '_blank');
        });
        let noteContainer = creationDesElements("div", conteneurImage, "stars");

        afficherEtoiles(noteContainer, o.id);

    });
}

function afficherEtoiles(conteneur, idArtiste) {
    conteneur.innerHTML = "";
    note = 0;

    notesArtistes = JSON.parse(localStorage.getItem(nomItem));
    notesArtistes.forEach(function (item) {
        if (item.id == idArtiste) {
            note = item.note;
            return true
        }
    })


    let i = 1;
    while (i < 6) {
        let starInput = creationDesElements("input", conteneur, "");
        starInput.name = "star";
        starInput.type = "radio";
        starInput.id = "star" + idArtiste + i;
        starInput.style.display = "none";
        let starLabel = creationDesElements("label", conteneur, "notes");
        if (note != 0 && i <= note) {
            starLabel.textContent = "★";

        } else {
            starLabel.textContent = "☆";

        }
        starLabel.htmlFor = starInput.id;
        starInput.addEventListener("click", function (evt) {
            noter(evt, conteneur, idArtiste);
        });
        i++;

    }
}

function noter(e, conteneur, id) {
    let event = e.target;
    let note = event.id.substr(event.id.length - 1);
    let isInStorage = false;

    notesArtistes = JSON.parse(localStorage.getItem(nomItem));

    //console.log(notesArtistes);
    notesArtistes.forEach(function (item) {
        if (item.id == id) {
            item.note = note;
            isInStorage = true;
            return true
        }
    })
    if (!isInStorage) {
        let obj = {
            id: id,
            note: note
        };
        notesArtistes.push(obj);
    };

    localStorage.setItem(nomItem, JSON.stringify(notesArtistes));
    afficherEtoiles(conteneur, id);
}