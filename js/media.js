
const modal = document.querySelector("#js-modale");
const modalOpenBtns = document.querySelectorAll(".js-open");
const modalCloseBtn = document.querySelector(".close-btn");

let galerie;


function loadJSON(url, callback) {

    // Créer un objet XMLHttpRequest
    const xhr = new XMLHttpRequest();

    // Surveiller les changements de status

    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.responseText);
            callback(data);
        }
    }

    xhr.open("GET", url, true);
    xhr.send();
}

// Appeler la fonction callback du JSON

loadJSON("data/images-galerie.json", loadForModal);

// Récupération des données du JSON

function loadForModal(data) {
    galerie = data.image;
    modalOpenBtns.forEach(function (modalOpenBtn) {
        //console.log(galerie);
        modalOpenBtn.addEventListener("click", function (e) {
            elemt = e.target;
            let id = e.target.dataset.id;
            let article = document.querySelector("#js-modale-article");
            let nom = document.querySelector("#js-title");

            nom.textContent = galerie[id].titre;
            if (galerie[id].type == "img") {
                article.innerHTML = "";
                let imgContainer = creationDesElements("div",article,"#js-img-container")
                imgContainer.style.display = "block";
                let img = creationDesElements("img",imgContainer,"#img-modal")
                img.src = galerie[id].link;
                img.alt= galerie[id].alt;     
                console.log(img);
            } else {
                article.innerHTML = "";
                let videoContainer = creationDesElements("div",article,"#js-video-container");
                videoContainer.style.display = "flex";
                let video = creationDesElements("iframe",videoContainer,"#js-video");
                video.src = galerie[id].link;
            }
            modal.style.display = "flex";
        });
    })
}

// Fonction nécessaire pour afficher les éléments sur ma page

function creationDesElements(type, conteneur, style) {
    let b = document.createElement(type);
    conteneur.appendChild(b);
    console.log(style.substr(1));
    if (style != "" && style.substr(0) == "#") {
        b.classList.add(style);
    }else if(style.substr(0,1) == "#"){
        
        b.id = style.substr(1);
    }
    return b;
}






modalCloseBtn.addEventListener("click", function () {
    modal.style.display = "none";
});

modal.addEventListener("click", function (evt) {
    if (evt.target.id == "js-modale") {
        modal.style.display = "none";
    }
})